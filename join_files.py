from os import listdir
from os.path import isfile, join

path = "./haskell/"
name = "everything.hs"
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

lista = []
for i in onlyfiles:
    f = open(path + str(i), "r")
    lista.append(f.read())
    lista.append("\n \n")
    f.close()

everything = open(path + name, "w")
for i in lista:
    everything.write(i)
everything.close()